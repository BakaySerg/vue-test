import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
   state: {
      lists: [
            {
               title: "Modularity",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit... ",
               imageSrc: "img/services/6.svg",
               id: "1"
            },
            {
               title: "Versatile",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia.",
               imageSrc: "img/services/2.svg",
               id: "2"
            },
            {
               title: "Responsive",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/3.svg",
               id: "3"
            },
            {
               title: "Configurable",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit.",
               imageSrc: "img/services/1.svg",
               id: "4"
            },
            {
               title: "Performant",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia, quis blandit eros facil. ",
               imageSrc: "img/services/5.svg",
               id: "5"
            },
            {
               title: "Easy to unfold",
               description:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam consequat arcu sed nulla lacinia blandit. ",
               imageSrc: "img/services/4.svg",
               id: "6"
            },
         ]

   }
})